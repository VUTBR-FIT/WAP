var initStyleOn = true;
function dynamicTable(id) {
    initStyles();
    var table = document.getElementById(id);
    var head = table.getElementsByTagName('thead');
    var body = table.getElementsByTagName('tbody');

    
    var struct = {mapping: [], cells: [], resize: {
            divs: [], row: 0, col: 0, enable: false, start: {x: 0.0, y:0.0, width: 0.0, height: 0.0}
                }, head: 0, table: table, drag: {src: null, dest: null, dir: 0, triangle: null} };
    var source = {
        head: (head !== null ? head[0] : null),
        body: (body !== null ? body[0] : null)
    };
    document.addEventListener("mouseup", onMouseUp);
    document.addEventListener("mousemove", onMouseMoveResize);
    table.addEventListener("mouseenter", function() {
        if (!struct.resize.enable) {
            initMappingCells();
            initResize();
        }
    });
    table.addEventListener("mouseleave", function() {
        if (!struct.resize.enable) {
            destroyResize();
            destroyMappingCells();
        }
    });
    /**
     * Handler for relased mouse button
     */
    function onMouseUp() {
        if (struct.drag.src !== null && struct.drag.dest !== null) {
            var dragPosition = findCell(struct.drag.src);
            var currentPosition = findCell(struct.drag.dest);
            if (dragPosition !== currentPosition) {
                var lastColl = getColSpan(dragPosition.row, currentPosition.col) + currentPosition.col - 1;
                for (var row = dragPosition.row; row < struct.cells.length; row++) {
                    var lastCell = (struct.drag.dir === 0)?struct.cells[row][currentPosition.col]:struct.cells[row][lastColl];
                    for (var col = dragPosition.col; col < (getColSpan(dragPosition.row, dragPosition.col) + dragPosition.col); col++) {
                        var cell = struct.cells[row][col];
                        if (typeof(cell) === 'undefined')
                            continue;
                        if (struct.drag.dir === 0) {//left
                            lastCell.parentNode.insertBefore(cell, lastCell);
                        } else {//right
                            lastCell.parentNode.insertBefore(cell, lastCell.nextSibling);
                            lastCell = cell;
                        }
                    }
                }
            }
            
            destroyResize();
            destroyMappingCells();
            initMappingCells();
            initResize();
        }
        if (struct.drag.triangle !== null) {
            struct.drag.triangle.parentNode.removeChild(struct.drag.triangle);
            struct.drag.triangle = null;
        }
        struct.drag.src = null;
        struct.drag.dest = null;
        if (struct.resize.enable)
            struct.resize.enable = false;
    }
    /**
     * Will record the current cursor position in table head when dragging
     * 
     * @param {Event} env Triggered event
     */
    function onMouseMove(env) {
        if (struct.drag.src !== null) {
            var dragPosition = findCell(struct.drag.src);
            var currentPosition = findCell(this);
            if (dragPosition.row === currentPosition.row) {
                var cellPosition = this.getBoundingClientRect();
                if (env.x < (cellPosition.left + (cellPosition.right - cellPosition.left)/2))
                    struct.drag.dir = 0;//left
                else 
                    struct.drag.dir = 1;//right
                // redraw triangle
                struct.drag.triangle.parentNode.removeChild(struct.drag.triangle);
                drawTriangleByPosition(env.x, cellPosition.left, cellPosition.right, cellPosition.y); 
            }
        }
    }
    /**
     * Will record moving into head cell when dragging
     */
    function onMouseEnter() {
        if (struct.drag.src !== null) {
            var dragPosition = findCell(struct.drag.src);
            var currentPosition = findCell(this);
            if (dragPosition.row === currentPosition.row)
                struct.drag.dest = this;
        }
    }
    /**
     * Will record mouse clicking cell in the table head
     * 
     * @param {Event} env Triggered event
     */
    function onMouseDown(env) {
        var position = findCell(this);
        if (position.row < struct.head) {
            struct.drag.src = this;
            var data = this.getBoundingClientRect();
            drawTriangleByPosition(env.x, data.left, data.right, data.top);
        }
    }
    /**
     * Will record mouse clicking border cell in the table
     * 
     * @param {Event} env Triggered event
     */
    function onMouseDownResize(env) {
        struct.resize.enable = true;
        struct.resize.col = Number(this.getAttribute('data-col'));
        struct.resize.row = Number(this.getAttribute('data-row'));
        struct.resize.direction = Number(this.getAttribute('data-direction'));
        var currentCell = struct.cells[struct.resize.row][struct.resize.col].getBoundingClientRect();
        struct.resize.start.height = currentCell.height;
        struct.resize.start.width = currentCell.width;
        struct.resize.start.x = env.x;
        struct.resize.start.y = env.y;
    }
    /**
     * Will record mouse moving
     * 
     * @param {Event} env Triggered event
     */
    function onMouseMoveResize(env) {
        if (struct.resize.enable) {
            var diffX = 0.0;
            var diffY = 0.0;
            if (struct.resize.direction !== 1) {
                diffY = env.y - struct.resize.start.y;
            }
            if (struct.resize.direction !== 0) {
                diffX = env.x - struct.resize.start.x;
            }
            updateStyle(struct.resize.row, struct.resize.col, struct.resize.start.width + diffX, struct.resize.start.height + diffY);
            destroyResize();
            initResize();
        }
    }
    /**
     * Get coll span of cell
     * 
     * @param {Number} row 
     * @param {Number} col
     * @returns {Number}
     */
    function getColSpan(row, col) {
        var result = 0;
        if (row in struct.mapping) {
            for (var i = 0; i < struct.mapping[row].length; i++) {
                if (struct.mapping[row][i] === col)
                    result++;
            }
        }
        return result;
    }
    /**
     * Draw tringle in position by cursor
     * 
     * @param {Object} xCursor
     * @param {Number} xLeft
     * @param {Number} xRight
     * @param {Number} y
     */
    function drawTriangleByPosition(xCursor, xLeft, xRight, y) {
        if (xCursor > (xLeft + (xRight-xLeft)/2))
            drawTriangle(xRight, y);
        else
            drawTriangle(xLeft, y);
    }
    /**
     * Draw triangle in position
     * 
     * @param {Number} x
     * @param {Number} y
     */
    function drawTriangle(x, y) {
        var obj = document.createElement('div');
        struct.drag.triangle = obj;
        document.body.appendChild(obj);
        obj.style.width = "0px";
        obj.style.height = "0px";
        obj.style.position = "absolute";
        obj.style["border-left"] = "6px solid transparent";
        obj.style["border-right"] = "6px solid transparent";
        obj.style["border-top"] = "6px solid #f00";
        obj.style["z-index"] = 100;
        obj.style.top = (y-6)  + "px";
        obj.style.left = (x-6) + "px";
    }
    /**
     * Mapping table
     */
    function initMappingCells() {
        if (head !== null) {
            var rows = head[0].getElementsByTagName('tr');
            for (var row = 0; row < rows.length; row++) {
                struct.mapping.push([]);
                var cols = rows[row].getElementsByTagName('th');
                if (cols.length === 0)
                    cols = rows[0].getElementsByTagName('td');
                var span = 0;
                var shift = 0;
                for (var col = 0; col < cols.length; col++) {
                    if (cols[col].getAttribute('colspan') !== null)
                        span = Number(cols[col].getAttribute('colspan'));
                    else
                        span = 1;            
                    for(var i = 0; i < span; i++)  {
                        struct.mapping[row].push(shift);
                    }
                    shift += span;
                }
            }
        }
        var rowCounter = 0;
        for (var key in source) {
            if (source[key] === null)
                continue;
            var rows = source[key].getElementsByTagName('tr');
            for (var i = 0; i < rows.length; i++) {
                struct.cells.push({});
                var row = rows[i];

                if (key === 'head') {
                    var cols = row.getElementsByTagName('th');
                    struct.head++;
                }
                if (key === 'body' || cols.length === 0)
                    var cols = row.getElementsByTagName('td');

                var colSpan = 0;
                for (j = 0; j < cols.length; j++) {
                    var cell = cols[j];
                    struct.cells[rowCounter][j + colSpan] = cell;
                    if (key === 'head') {
                        if (i < 1) {
                            cell.style.cursor = "move";
                            cell.setAttribute('class', 'no-select');
                            cell.addEventListener("mousedown", onMouseDown);
                            cell.addEventListener("mousemove", onMouseMove);
                            cell.addEventListener("mouseenter", onMouseEnter);
                        }
                        if (cell.getAttribute('colspan') !== null)
                            colSpan = Number(cell.getAttribute('colspan')) - 1;
                    }
                }
                rowCounter++;
            }
        }
    }
    /**
     * Destroy mapped table
     */
    function destroyMappingCells() {
        struct.mapping = [];
        for (var row = 0; row < struct.cells.length; row++) {
            for (var col in struct.cells[row]) {
                var cell = struct.cells[row][col];
                cell.removeEventListener("mousedown", onMouseDown);
                cell.removeEventListener("mousemove", onMouseMove);
                cell.removeEventListener("mouseenter", onMouseEnter);
                document.addEventListener("mouseup", onMouseUp);
            }
        }
        struct.cells = [];
        struct.head = 0;
    }
    /**
     * Generate divs for resize rows and columns
     */
    function initResize() {
        for (var row = 0; row < struct.cells.length; row++) {
            struct.resize.divs.push({});
            for (var col in struct.cells[row]) {
                struct.resize.divs[row][col] = {};
                var position = struct.cells[row][col].getBoundingClientRect();
                var div = initResizeDiv(position.left, position.bottom, position.width, position.height, 0, row === 0 || 0 === col);
                struct.resize.divs[row][col].down = div;
                div.setAttribute('data-row', row);
                div.setAttribute('data-col', col);
                div.addEventListener("mousedown", onMouseDownResize);
                //DEBUG div.style['background-color'] = 'blue';
                div = initResizeDiv(position.right, position.top, position.width, position.height, 1, row === 0 || 0 === col);
                struct.resize.divs[row][col].right = div;
                div.setAttribute('data-row', row);
                div.setAttribute('data-col', col);
                div.addEventListener("mousedown", onMouseDownResize);
                //DEBUG div.style['background-color'] = 'green';
                div = initResizeDiv(position.right, position.bottom, position.width, position.height, 2, row === 0 || 0 === col);
                struct.resize.divs[row][col].corner = div;
                div.setAttribute('data-row', row);
                div.setAttribute('data-col', col);
                div.addEventListener("mousedown", onMouseDownResize);
                //DEBUG div.style['background-color'] = 'red';
            }
        }
    }
    /**
     * Remove divs for resize
     */
    function destroyResize() {
        for (var row = 0; row < struct.cells.length; row++) {
            for (var col in struct.cells[row]) {
                for (var div in struct.resize.divs[row][col]) {
                    var obj = struct.resize.divs[row][col][div];
                    obj.parentNode.removeChild(obj);
                }
            }
        }
        struct.resize.divs = [];
    }
    /**
     * Generate div for resize
     * 
     * @param {Float} x Left position
     * @param {FLoat} y Top postion
     * @param {Float} width Width of generated div
     * @param {Float} height Height of generated div
     * @param {Number} orientation 0 - vodorovne, 1 - svisle, 2 - roh
     * @param {Boolean} isFirst Is true if is first col or row
     * @returns {undefined}  */
    function initResizeDiv(x, y, width, height, orientation, isFirst) {
        //fixed scrolling (https://plainjs.com/javascript/styles/get-the-position-of-an-element-relative-to-the-document-24/)
        scrollLeft = window.pageXOffset || document.documentElement.scrollLeft;
        scrollTop = window.pageYOffset || document.documentElement.scrollTop;
        
        x = x + scrollLeft;
        y = y + scrollTop;
        
        var obj = document.createElement('div');
        struct.table.appendChild(obj);
        obj.style['z-index'] = 100;
        var size = 2.0;
        var padding = (isFirst?0:size);
        obj.style.position = 'absolute';
        obj.style.width = (2*size) + "px";
        obj.style.height = (2*size) + "px";
        switch (orientation) {
            case 0:
                obj.style.top = (y - size) + "px";
                obj.style.left = (x + padding) + "px";
                obj.style.width = (width - (isFirst?1:2)*size) + "px";
                obj.style.cursor = 'n-resize';
                break;
            case 1:
                obj.style.top = (y + padding) + "px";
                obj.style.left = (x - size) + "px";
                obj.style.height = (height - (isFirst?1:2)*size) + "px";
                obj.style.cursor = 'e-resize';
                break;
            default:
                obj.style.top = (y - size) + "px";
                obj.style.left = (x - size) + "px";
                obj.style['z-index'] = 101;
                obj.style.cursor = 'nw-resize';
                break;
        }
        obj.setAttribute('data-direction', orientation);
        return obj;
    }
    /**
     * Update width and height of cells
     * 
     * @param {Number} row
     * @param {Number} col
     * @param {Float} width
     * @param {Float} height
     */
    function updateStyle(row, col, width, height) {
        // Set height of cells in table
        for (var key in struct.cells[row]) {
            struct.cells[row][key].style.height = height + 'px';
        }
        // Set width of cells in table body
        for (var i = struct.head; i < struct.cells.length; i++) {
            struct.cells[i][col].style.width = width + 'px';
        }
        // Set width of cells in table head
        for (var i = 0; i < struct.head; i++) {
            if (typeof (struct.cells[i][col]) !== 'undefined') {
                if (struct.cells[i][col].getAttribute('colspan') === null) {
                    struct.cells[i][col].style.width = width + 'px';
                    continue;
                }
            }

            var tempColl = col;
            while (typeof (struct.cells[i][tempColl]) === 'undefined') {
                tempColl--;
            }
            var cell = struct.cells[i][tempColl];
            cell.style.width = getWidthOfCells(struct.head, tempColl, tempColl + Number(cell.getAttribute('colspan')) - 1) + 'px';
        }
    }
    /**
     * Find the position of cell
     * 
     * @param {Object} obj Searched cell
     * @returns {row: Number, col: Nubmer} 
     */
    function findCell(obj) {
        for (var row = 0; row < struct.cells.length; row++) {
            for (var col in struct.cells[row]) {
                if (struct.cells[row][col] === obj)
                    return {row: row, col: Number(col)};
            }
        }
    }
    /**
     * Count width of the cells on the row and in rande of startCol and endCol
     * 
     * @param {Number} row
     * @param {Number} startCol First column
     * @param {Number} endCol Last column
     * @returns {String} Width of columns
     */
    function getWidthOfCells(row, startCol, endCol) {
        var result = 0;
        var enable = false;
        for (var col in struct.cells[row]) {
            if (col === startCol || enable) {
                enable = true;
                if (struct.cells[row][col].style.width === '')
                    result = result + struct.cells[row][col].offsetWidth;
                else
                    result = result + pxToFloat(struct.cells[row][col].style.width);
            }
            if (col === endCol) break;
        }
        return result + "px";
    }
    /**
     * Converts number in px to float
     * 
     * @param {String} source Number in px
     * @returns {Float}
     */
    function pxToFloat(source) {
        var result = "";
        for (var i = 0; i < source.length; i++) {
            if ((Number(source[i]) >= 0 && Number(source[i]) <= 9) || source[i] === '.')
                result = result + source[i];
        }
        return parseFloat(result);
    }
    /**
     * Init styles for this component
     */
    function initStyles() {
        console.log(initStyleOn);
        if  (initStyleOn === true) {
            initStyleOn = false;
            var css = ".no-select {\
    -webkit-touch-callout: none; /* iOS Safari */\
    -webkit-user-select: none; /* Safari */\
    -khtml-user-select: none; /* Konqueror HTML */\
    -moz-user-select: none; /* Firefox */\
    -ms-user-select: none; /* IE & Edge */\
    -user-select: none; /* Chrome & Opera */\
}";
            var styles = document.createElement('style');
            styles.type = 'text/css';
            if (styles.styleSheet)
                styles.styleSheet.cssText = css;
            else
                styles.appendChild(document.createTextNode(css));
            
            document.body.appendChild(styles);
        }
    }
}